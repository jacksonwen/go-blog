import LoginPage from "@/views/login/LoginPage";

export const routes = [
    {
        path: '/login',
        element: <LoginPage />
    },
]