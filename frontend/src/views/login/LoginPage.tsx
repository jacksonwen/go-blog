
import { useState } from "react";
import { useImmer } from "use-immer";

export interface LoginForm {
  username?: string,
  password?: string
}

const useLogin = () => {
  const [loginForm, setLoginForm] = useImmer<LoginForm>({
    username: '',
    password: ''
  })

  const submit = (form = loginForm) => {
    console.log(form);

  }

  return {
    loginForm,
    setLoginForm,
    submit
  }

}

const LoginPage = () => {
  const { loginForm, setLoginForm, submit } = useLogin()

  return (
    <div className="bg-gray-200 h-screen w-screen flex justify-center items-center">
      <form onSubmit={e => {
        e.preventDefault()
        submit()
      }} className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 w-1/3">
        <div className="mb-4">
          <label htmlFor="username" className="block text-gray-700 text-sm  font-bold mb-2">username: </label>
          <input type="text" className="form-input rounded shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="username" id="username" onInput={(e: any) => setLoginForm(draft => {
            draft.username = e.target.value

          })} value={loginForm.username} />
        </div>
        <div className="mb-4">
          <label htmlFor="password" className="block text-gray-700 text-smm font-bold mb-3">password: </label>
          <input type="password" className="form-input rounded shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-inner" value={loginForm.password} name="password" id="password" onInput={(e: any) => setLoginForm(draft => {
            draft.password = e.target.value
          })} />
        </div>
        <div>
          <button type="submit" className="bg-blue-500 hover:bg-blue-700 rounded-md font-bold text-sm text-white py-2 px-4 focus:outline-none focus:shadow-inner">Sign In</button>
        </div>
      </form>
    </div>
  )
}

export default LoginPage

